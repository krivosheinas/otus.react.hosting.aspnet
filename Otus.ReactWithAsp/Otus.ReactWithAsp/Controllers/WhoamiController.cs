﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.ReactWithAsp.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WhoamiController : ControllerBase
    {
       
        [HttpGet]
        public IActionResult Get()
        {
            return Ok(new { Name = "Андрей", LastName = "Кривошеин", Course = "C# ASP.NET Core разработчик" });
        }
    }
}
