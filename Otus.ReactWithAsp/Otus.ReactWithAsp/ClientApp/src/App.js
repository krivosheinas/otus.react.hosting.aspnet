import React,  { useState } from 'react';

function App() {

  const [value, setValue] = useState(0);
  const [whoAmi, setWhoami] = useState({})
  const [weather, setWeather] = useState({})
  const whoamiClickHandler = () =>{
    setValue(1)

    try{
      fetch("/whoami", {method: 'get'}).then(
        async res => {
          setWhoami(await res.json());
        }  
      )
    }catch(e){
      setWhoami({error: "Что-то пошло не так!"});  
    }
  }

  const weatherClickHandler = () =>{
    setValue(2)
    try{
      fetch("/weatherforecast", {method: 'get'}).then(
        async res => {
          setWeather(await res.json());
        }  
      )
    }catch(e){
      setWeather({error: "Что-то пошло не так!"});  
    }
  }

  return (  
    <div className="App">
      <h1>ReactApp + AspNet</h1>
      <div className="row">
        <div className="col">
          <button className='btn btn-primary mr-2' onClick={whoamiClickHandler}>Whoami</button>
          <button className='btn btn-primary' onClick={weatherClickHandler}>Weather Check</button>
        </div>       
      </div>

      { value === 1 ?
       <div className="Whoami">
         <h1>WHOAMI</h1>
         <b>{JSON.stringify(whoAmi)}</b>
       </div>
       : value === 2 ? 

       <div className="Wheather">
        <h1>WHEATHERCHECK</h1>
       
        { weather && weather.length > 0 ?
          <table className='table'> 
            <thead>
              <tr>
                <th scope="col">date</th>
                <th scope="col">temperatureC</th>
                <th scope="col">temperatureF</th>
                <th scope="col">summary</th>
              </tr>
            </thead>
            <tbody>
            {weather.map( (item, index) => 
            <tr key={index}>
              <td>{item.date}</td>
              <td>{item.temperatureC}</td>
              <td>{item.temperatureF}</td>
              <td>{item.summary}</td> 
            </tr>)
            }
            </tbody>

          </table> 
          :
          null 
        }
        
       </div>
       :
       null 
      }


    </div>      
  );
}

export default App;